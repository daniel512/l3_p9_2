﻿using System;

namespace P3_L9_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Reklama r1 = new Reklama("To jest reklama", GrupaWiekowa.starzy, Zainteresowania.sport);

            Console.WriteLine($"Grupa wiekowa: {r1.GrupaWiekowa}, zainteresowania {r1.Zainteresowania}");

            string zdanie = "jakieś tam zdanie";
            Console.WriteLine($"Ilość znaku 'a' w zdaniu '{zdanie}' to: {zdanie.IloscZnakow('a')}");

            Console.WriteLine(r1.CzyDlaDzieci());
        }
    }
}
