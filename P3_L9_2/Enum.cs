﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P3_L9_2
{
    [Flags]
    public enum GrupaWiekowa 
    {
        dzieci,
        młodzi,
        średni,
        starzy
    }

    public enum Zainteresowania
    {
        samochody,
        sport,
        gry
    }
}
