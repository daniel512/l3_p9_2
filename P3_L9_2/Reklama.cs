﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P3_L9_2
{
    class Reklama
    {
        public string Text { get; set; }
        public GrupaWiekowa GrupaWiekowa { get; set; }
        public Zainteresowania Zainteresowania { get; set; }

        public Reklama(string text, GrupaWiekowa grupa, Zainteresowania zainteresowania)
        {
            this.Text= text;
            this.GrupaWiekowa = grupa;
            this.Zainteresowania = zainteresowania;
        }

        public bool CzyDlaDzieci()
        {
            if (this.GrupaWiekowa == GrupaWiekowa.dzieci || this.GrupaWiekowa == GrupaWiekowa.młodzi)
            {
                return true;
            }
            else return false;
        }
    }
}
