﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P3_L9_2
{
    public static class ExtensionMethods
    {

        public static int IloscZnakow(this string wyraz, char znak)
        {
            int sumaZnakow = 0;
            for (int i = 0; i < wyraz.Length; i++)
            {
                if (wyraz[i] == znak)
                {
                    sumaZnakow++;
                }
            }
            return sumaZnakow;
        }
        public static (int, int) DzielenieZReszta(this int liczba, int dzielnik)
        {
            return (liczba / dzielnik, liczba % dzielnik);
        }

       
    }
}
